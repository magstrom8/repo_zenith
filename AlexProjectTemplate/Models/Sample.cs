﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AlexProjectTemplate.Models
{
    public class Sample
    {
        [Key]
        public string Id { set; get; }
        public string Name { set; get; }
        public string Address { set; get; }
    }
}